(ns casey.slack
  (:gen-class)
  (:require [clojure.data.json :as json]
            [clj-http.client :as client]))


(defn build-attachment "Build Slack message attachment"
  [config item]
  {:fallback (format "%s (%s)", (:title item) (:link item))
   :text (format "<%s|%s>" (:link item) (:title item))
   :color (-> config :slack :color)
   :fields [{:title "Category"
             :short true
             :value (first (:categories item))}
            {:title "Opened"
             :short true
             :value (str (:published item))}]})

(defn build-message "Build Slack message"
  [config attachments]
  {:channel (-> config :slack :recipient)
   :username (-> config :slack :username)
   :text (-> config :slack :introText)
   :fallback (-> config :slack :introText)
   :attachments attachments})

(defn send-message "Send message to Slack (via webhook)"
  [config msg]
  (client/post (-> config :slack :webhookUrl)
               {:content-type :json
                :body (json/write-str msg)}))