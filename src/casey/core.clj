(ns casey.core
  (:gen-class)
  (:require [clojure.data.json :as json]
            feedme
            [casey.slack :as slack]))

(defn read-config "Reads and parses JSON config and returns a hash map"
  []
  (json/read-str (slurp (clojure.java.io/resource "config.json")) :key-fn keyword))

(defn -main "The thing... again..."
  [& args]
  (try
    (let [config (read-config)]
      (slack/send-message config
        (slack/build-message config
          (into [] (for [i (:entries (feedme/parse (-> config :fogbugz :feedUrl)))]
            (slack/build-attachment config i))))))
            (catch Exception e
              (println (format "Exception caught: %s" (str e))))))

