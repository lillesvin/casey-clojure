# Casey Jones (Clojure)

Small tool that posts cases from a FogBugz filter in Slack. This is the Clojure version.

Copy `resources/config.json.sample` to `resources/config.json` and put your settings there.
