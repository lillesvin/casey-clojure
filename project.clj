(defproject casey "0.2.0"
  :description "Check FogBugz RSS feed for new cases"
  :url "http://example.com/FIXME"
  :license {:name "Apache-2.0"
            :url "http://www.apache.org/licenses/LICENSE-2.0"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.6"]
                 [feedme "0.0.3"]
                 [clj-http "3.9.1"]]
  :main ^:skip-aot casey.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
